#include "gtest/gtest.h"

class DummyPluginTest : public ::testing::Test {

protected:

    DummyPluginTest() {

    }

    ~DummyPluginTest() override {

    }

    void SetUp() override {

    }

    void TearDown() override {

    }
};

TEST(DummyPluginTest, TestName) {
    ASSERT_EQ(1, 1);
}

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
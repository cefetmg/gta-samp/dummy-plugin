#include "gmock/gmock.h"
#include "model/Vehicle.hpp"
#include "service/Validator.hpp"
#include <iostream>

namespace gsamp {
    namespace service {
        class ValidatorMock : public Validator<gsamp::model::Vehicle> {

            public:

                MOCK_CONST_METHOD1(FunctorOperator, bool(const gsamp::model::Vehicle&));

                virtual bool operator()(const gsamp::model::Vehicle& vehicle) const override { 
                    return FunctorOperator(vehicle); 
                }
        };
    }
}
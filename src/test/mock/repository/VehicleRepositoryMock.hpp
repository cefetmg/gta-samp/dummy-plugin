#include "gmock/gmock.h"
#include "repository/AbstractRepository.hpp"
#include "model/Vehicle.hpp"
#include <memory>

namespace gsamp {
    namespace repository {
        class VehicleRepositoryMock : public AbstractRepository<gsamp::model::Vehicle, long> {
            public:
            
                MOCK_METHOD(std::shared_ptr<gsamp::model::Vehicle>, create, (gsamp::model::Vehicle), (override));

                MOCK_METHOD(std::shared_ptr<gsamp::model::Vehicle>, update, (gsamp::model::Vehicle), (override));

                MOCK_METHOD(void, remove, (long), (override));

                MOCK_METHOD(std::shared_ptr<gsamp::model::Vehicle>, findOne, (long), (override));

                MOCK_METHOD(std::vector<gsamp::model::Vehicle>, findAll, (), (override));
        };
    }
}

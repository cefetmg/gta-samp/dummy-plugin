#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "service/VehicleService.hpp"
#include "service/Validator.hpp"
#include "repository/AbstractRepository.hpp"
#include "mock/repository/VehicleRepositoryMock.hpp"
#include "mock/service/ValidatorMock.hpp"

gsamp::service::VehicleService* vehicleService;

class VehicleServiceTest : public ::testing::Test {

    VehicleServiceTest() {

    }

    ~VehicleServiceTest() override {

    }

    void SetUp() override {

    }
    
    void TearDown() override {
        delete vehicleService;
    }

};

TEST(VehicleServiceTest, ShouldReturnVehicleWithIdWhenSaving) {
    //given
    std::shared_ptr<gsamp::model::Vehicle> vehicleWithId(new gsamp::model::Vehicle(std::shared_ptr<long>(new long(1L))));
    gsamp::service::ValidatorMock createValidatorMock;
    gsamp::service::ValidatorMock updateValidatorMock;
    gsamp::repository::VehicleRepositoryMock abstractRepositoryMock;
    EXPECT_CALL(createValidatorMock, FunctorOperator(::testing::_))
        .Times(1).WillOnce(::testing::Return(true));
    EXPECT_CALL(abstractRepositoryMock, create(::testing::_))
        .Times(1).WillOnce(::testing::Return(vehicleWithId));
    vehicleService = new gsamp::service::VehicleService(createValidatorMock, updateValidatorMock, abstractRepositoryMock);

    //when
    std::shared_ptr<gsamp::model::Vehicle> retVehicle = vehicleService->createVehicle(gsamp::model::Vehicle());
    
    //then
    ASSERT_EQ(*(*retVehicle).getId(), *(*vehicleWithId).getId());
}

TEST(VehicleServiceTest, ShouldThrowExceptionWhenTryingToSaveVehicleWithId) {
    //given
    gsamp::service::ValidatorMock createValidatorMock;
    gsamp::service::ValidatorMock updateValidatorMock;
    gsamp::repository::VehicleRepositoryMock abstractRepositoryMock;
    gsamp::model::Vehicle vehicle(std::shared_ptr<long>(new long(1L)));
    EXPECT_CALL(createValidatorMock, FunctorOperator(::testing::_))
        .Times(1).WillOnce(::testing::Return(false));
    EXPECT_CALL(abstractRepositoryMock, create(::testing::_)).Times(0);
    vehicleService = new gsamp::service::VehicleService(createValidatorMock, updateValidatorMock, abstractRepositoryMock);

    //when
    try {
        vehicleService->createVehicle(gsamp::model::Vehicle());
    } catch(std::string& ex) {
        ASSERT_EQ("Validation errors were found", ex);
    }
}

TEST(VehicleServiceTest, ShouldReturnVehicleWithIdWhenUpdating) {
    //given
    std::shared_ptr<gsamp::model::Vehicle> vehicleWithId(new gsamp::model::Vehicle(std::shared_ptr<long>(new long(1L))));
    gsamp::service::ValidatorMock createValidatorMock;
    gsamp::service::ValidatorMock updateValidatorMock;
    gsamp::repository::VehicleRepositoryMock abstractRepositoryMock;
    EXPECT_CALL(updateValidatorMock, FunctorOperator(::testing::_))
        .Times(1).WillOnce(::testing::Return(true));
    EXPECT_CALL(abstractRepositoryMock, update(::testing::_))
        .Times(1).WillOnce(::testing::Return(vehicleWithId));
    vehicleService = new gsamp::service::VehicleService(createValidatorMock, updateValidatorMock, abstractRepositoryMock);

    //when
    std::shared_ptr<gsamp::model::Vehicle> retVehicle = vehicleService->updateVehicle(*vehicleWithId);
    
    //then
    ASSERT_EQ(*(*retVehicle).getId(), *(*vehicleWithId).getId());
}

TEST(VehicleServiceTest, ShouldThrowExceptionWhenTryingToUpdateVehicleWithNoId) {
    //given
    gsamp::service::ValidatorMock createValidatorMock;
    gsamp::service::ValidatorMock updateValidatorMock;
    gsamp::repository::VehicleRepositoryMock abstractRepositoryMock;
    gsamp::model::Vehicle vehicle(std::shared_ptr<long>(new long(1L)));
    EXPECT_CALL(updateValidatorMock, FunctorOperator(::testing::_))
        .Times(1).WillOnce(::testing::Return(false));
    EXPECT_CALL(abstractRepositoryMock, update(::testing::_)).Times(0);
    vehicleService = new gsamp::service::VehicleService(createValidatorMock, updateValidatorMock, abstractRepositoryMock);

    //when
    try {
        vehicleService->updateVehicle(gsamp::model::Vehicle());
    } catch(std::string& ex) {
        ASSERT_EQ("Validation errors were found", ex);
    }
}

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
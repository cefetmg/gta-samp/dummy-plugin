#include "gtest/gtest.h"
#include "model/Vehicle.hpp"
#include "service/VehicleCreateFieldsValidator.hpp"
#include <memory>

gsamp::service::Validator<gsamp::model::Vehicle>& validator = *(new gsamp::service::VehicleCreateFieldsValidator());

class VehicleCreateFieldsValidatorTest : public ::testing::Test {

protected:

    VehicleCreateFieldsValidatorTest() {

    }

    ~VehicleCreateFieldsValidatorTest() override {

    }

    void SetUp() override {

    }
    
    void TearDown() override {
        delete &validator;
    }

};

TEST(VehicleCreateFieldsValidatorTest, ShouldReturnTrueWhenVehicleHasNoId) {
    //given
    gsamp::model::Vehicle vehicle;

    //when
    bool returnValue = validator(vehicle);

    //then
    ASSERT_TRUE(returnValue) << "Vehicle's id: " << vehicle.getId().get();
}

TEST(VehicleFieldsValidatorTest, ShouldReturnFalseWhenVehicleHasId) {
    //given
    gsamp::model::Vehicle vehicle(std::shared_ptr<long>(new long(1L)));

    //when
    bool returnValue = validator(vehicle);

    //then
    ASSERT_FALSE(returnValue) << "Vehicle's id: " << *vehicle.getId();
}

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
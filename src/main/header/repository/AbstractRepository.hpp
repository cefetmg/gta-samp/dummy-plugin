#ifndef ABSTRACT_REPOSITORY_H
#define ABSTRACT_REPOSITORY_H

#include "repository/AbstractReadOnlyRepository.hpp"
#include "repository/AbstractWriteOnlyRepository.hpp"

namespace gsamp {
    namespace repository {

        template <typename Entity, typename EntityId>
        class AbstractRepository : public AbstractReadOnlyRepository<Entity, EntityId>, public AbstractWriteOnlyRepository<Entity, EntityId> {

        };
    }
}

#endif
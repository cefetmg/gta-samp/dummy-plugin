#ifndef ABSTRACT_READ_ONLY_REPOSITORY_H
#define ABSTRACT_READ_ONLY_REPOSITORY_H

#include <memory>
#include <vector>

namespace gsamp {
    namespace repository {

        template <typename Entity, typename EntityId>
        class AbstractReadOnlyRepository {

            public:

                virtual std::shared_ptr<Entity> findOne(EntityId) = 0;

                virtual std::vector<Entity> findAll() = 0;
        };
    }
}

#endif
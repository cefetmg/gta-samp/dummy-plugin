#ifndef ABSTRACT_WRITE_ONLY_REPOSITORY_H
#define ABSTRACT_WRITE_ONLY_REPOSITORY_H

#include <memory>

namespace gsamp {
    namespace repository {

        template <typename Entity, typename EntityId> 
        class AbstractWriteOnlyRepository {

            public:

                virtual std::shared_ptr<Entity> create(Entity) = 0;

                virtual std::shared_ptr<Entity> update(Entity) = 0;

                virtual void remove(EntityId) = 0;
        };
    }
}

#endif
#ifndef REPOSITORY_VEHICLE_REPOSITORY_H
#define REPOSITORY_VEHICLE_REPOSITORY_H

#include "repository/AbstractRepository.hpp"
#include "model/Vehicle.hpp"

namespace gsamp {
    namespace repository {

        class VehicleRepository : public AbstractRepository<gsamp::model::Vehicle, long> {
            
            public:

                std::shared_ptr<gsamp::model::Vehicle> create(gsamp::model::Vehicle) override;

                std::shared_ptr<gsamp::model::Vehicle> update(gsamp::model::Vehicle) override;

                void remove(long) override;

                std::shared_ptr<gsamp::model::Vehicle> findOne(long) override;

                std::vector<gsamp::model::Vehicle> findAll() override;
        };

    }
}

#endif
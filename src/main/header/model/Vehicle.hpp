#ifndef MODEL_VEHICLE_H
#define MODEL_VEHICLE_H

#include <memory>

namespace gsamp {
        namespace model {

        class Vehicle {

        private:

            std::shared_ptr<long> m_id;

        public:

            Vehicle();

            Vehicle(std::shared_ptr<long> id);

            ~Vehicle();

            std::shared_ptr<long> getId() const;

            void setId(std::shared_ptr<long> id);
        };
    }
}

#endif
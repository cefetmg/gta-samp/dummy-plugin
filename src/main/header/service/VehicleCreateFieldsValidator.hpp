#ifndef SERVICE_VEHICLE_CREATE_FIELDS_VALIDATOR_H
#define SERVICE_VEHICLE_CREATE_FIELDS_VALIDATOR_H

#include "service/Validator.hpp"
#include "model/Vehicle.hpp"

namespace gsamp {
    namespace service {

        class VehicleCreateFieldsValidator : public Validator<gsamp::model::Vehicle> {

        public:
            bool operator()(const gsamp::model::Vehicle&) const override;
        };
    }
}

#endif
#ifndef SERVICE_VALIDATOR_H
#define SERVICE_VALIDATOR_H

namespace gsamp {
    namespace service {

        template <typename ValidatingClass>
        class Validator {

            public:
                virtual bool operator()(const ValidatingClass&) const = 0;
        };
    }
}

#endif
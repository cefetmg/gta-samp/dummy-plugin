#ifndef VEHICLE_SERVICE_H
#define VEHICLE_SERVICE_H

#include <vector>
#include <memory>
#include "service/Validator.hpp"
#include "repository/AbstractRepository.hpp"
#include "model/Vehicle.hpp"

namespace gsamp {
    namespace service {
        class VehicleService {

            private:

                Validator<gsamp::model::Vehicle>& m_createValidator;

                Validator<gsamp::model::Vehicle>& m_updateValidator;

                gsamp::repository::AbstractRepository<gsamp::model::Vehicle, long>& m_repository;

            public:

                VehicleService(Validator<gsamp::model::Vehicle>& createValidator, 
                    Validator<gsamp::model::Vehicle>& updateValidator,
                    gsamp::repository::AbstractRepository<gsamp::model::Vehicle, long>& repository);

                ~VehicleService();

                std::shared_ptr<gsamp::model::Vehicle> createVehicle(gsamp::model::Vehicle);

                std::shared_ptr<gsamp::model::Vehicle> updateVehicle(gsamp::model::Vehicle);
        };
    }
}

#endif
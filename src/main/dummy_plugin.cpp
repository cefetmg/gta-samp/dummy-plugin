#include "amx.h"
#include "plugincommon.h"
#include <iostream>

typedef void (*logprintf_t)(const char* format, ...);

logprintf_t logprintf;

extern void *pAMXFunctions;

AMX_NATIVE_INFO PluginNatives[] = {
    {NULL, NULL}
};

PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() {
    return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES | SUPPORTS_PROCESS_TICK;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load(void** ppData) {
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t) ppData[PLUGIN_DATA_LOGPRINTF];
	logprintf("Loaded dummy_plugin");
	return true;
}

PLUGIN_EXPORT int PLUGIN_CALL Unload() {
	logprintf("Unloaded dummy_plugin");
	return 1;
}

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX* amx) {
	logprintf("Loading AMX script");
	return amx_Register(amx, PluginNatives, -1);
}


PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX* amx) {
	logprintf("Unloading AMX script");
	return AMX_ERR_NONE;
}

PLUGIN_EXPORT void PLUGIN_CALL ProcessTick() {
	static int tick = 0;
	if (!(tick % 100)) {
		logprintf("dummy_plugin v.2. has iterated over %d times", tick);
	}
	tick++;
}

const char* RetornaMensagem() {
	return "I worked!";
}
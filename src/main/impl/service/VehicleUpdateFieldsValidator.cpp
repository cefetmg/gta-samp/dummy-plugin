#include "service/VehicleUpdateFieldsValidator.hpp"
#include <iostream>

namespace gsamp {
    namespace service {

        bool VehicleUpdateFieldsValidator::operator()(const gsamp::model::Vehicle& vehicleInstance) const {
            return vehicleInstance.getId().get() != nullptr;
        }
    }
}
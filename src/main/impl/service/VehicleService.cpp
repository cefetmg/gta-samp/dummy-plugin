#include "service/VehicleService.hpp"
#include "service/Validator.hpp"
#include "service/VehicleCreateFieldsValidator.hpp"
#include "service/VehicleUpdateFieldsValidator.hpp"
#include <iostream>

namespace gsamp {
    namespace service {

        VehicleService::VehicleService(Validator<gsamp::model::Vehicle>& createValidator,
                    Validator<gsamp::model::Vehicle>& updateValidator,
                    gsamp::repository::AbstractRepository<gsamp::model::Vehicle, long>& repository) :  
            m_createValidator(createValidator),
            m_updateValidator(updateValidator),
            m_repository(repository) {
                
            }

        VehicleService::~VehicleService() {
            delete &m_createValidator;
            delete &m_updateValidator;
            delete &m_repository;
        }

        std::shared_ptr<gsamp::model::Vehicle> VehicleService::createVehicle(gsamp::model::Vehicle vehicleInstance) {
            if (m_createValidator(vehicleInstance)) {
                return m_repository.create(vehicleInstance);
            } else {
                throw std::string("Validation errors were found");
            }
        }

        std::shared_ptr<gsamp::model::Vehicle> VehicleService::updateVehicle(gsamp::model::Vehicle vehicleInstance) {
            
            if (m_updateValidator(vehicleInstance)) {
                return m_repository.update(vehicleInstance);
            } else {
                throw std::string("Validation errors were found");
            }
        }
    }
}
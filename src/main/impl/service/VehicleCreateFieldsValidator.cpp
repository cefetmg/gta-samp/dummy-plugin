#include "service/VehicleCreateFieldsValidator.hpp"
#include <iostream>

namespace gsamp {
    namespace service {

        bool VehicleCreateFieldsValidator::operator()(const gsamp::model::Vehicle& vehicleInstance) const {
            return vehicleInstance.getId().get() == nullptr;
        }
    }
}
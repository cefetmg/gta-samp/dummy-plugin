#include "model/Vehicle.hpp"

namespace gsamp {
    namespace model {

        Vehicle::Vehicle() { 
            m_id = std::shared_ptr<long>(nullptr);
        }

        Vehicle::Vehicle(std::shared_ptr<long> id): m_id(id) { }

        Vehicle::~Vehicle() {}

        std::shared_ptr<long> Vehicle::getId() const {
            return m_id;
        }

        void Vehicle::setId(std::shared_ptr<long> id) {
            m_id = id;
        }

    }
}

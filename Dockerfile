FROM debian

RUN apt-get update
RUN apt-get install -y g++ g++-multilib make cmake
RUN apt-get install -y python3 python3-pip
RUN pip3 install conan
RUN apt-get install -y git
RUN conan remote add dummy_plugin "https://api.bintray.com/conan/cefetmg/gtasa_samp"

WORKDIR /usr/src/dummy_plugin
ADD . /usr/src/dummy_plugin

RUN "bash"
from conans import ConanFile, CMake, tools

import os
import platform

class DummypluginConan(ConanFile):
    name = "DummyPlugin"
    user = "Cefetmg"
    license = "MIT"
    author = "Rodrigo Novaes rodrigo.novaes.jr@gmail.com"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "Dummy Plugin project for GTA-SA/SA-MP proof of concepts"
    topics = ("Autonomous Vehicles", "VANETs", "IOT", "CEFET-MG")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    generators = "cmake"
    scm = {
        "type": "git",
        "subfolder": "",
        "url": "https://gitlab.com/cefetmg/gta-samp/dummy-plugin",
        "revision": "master",
        "username": os.environ.get('GIT_USERNAME'),
        "password": os.environ.get('GIT_PASSWORD')
    }

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")

        if (platform.system() == "Windows"):
            cmake.configure(args="-A Win32")
        
        cmake.build()

    def package(self):
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*dummy_plugin.so", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["dummy_plugin"]


# A dummy plugin for GTA-SA/SA-MP builds

This project is merely an example on how to build plugins for GTA-SA/SA-MP platform.

## 1. Building

This project can be built using `cmake:3.10` or above. Just run

```Shell
mkdir build && cd build
cmake ..
make
```

In Windows, please run

```Shell
mkdir build
cd build
cmake .. -A Win32
cmake --build .
```

## 2. Project Structure

This version uses a git submodule to statically link SA-MP Plugin SDK to your project. By using this project as a template, no changes should be done in future plugins.

## References
